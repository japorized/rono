# rono

rono is a small library that provides common utilities to writing rofi scripts.
The name is an awful acronym made from [*ro*fi](https://github.com/davatorium/rofi) and [de*no*](https://deno.land). I blame my naming sense.

If you already know how to write scripts meant for rofi, then you already know more than half of how to use this library.

If you don't know how to write scripts for rofi, `man rofi-script` is your best friend.

---

## Usage


---

## API

- [x] Mode options
- [ ] Row options

---

## Important Tips

### Changing delims

Changing delims make things a quite difficult to make any good API design by this lib. If you're not aware, if you'd like to have different parts of your script use different delims, since rofi remembers which delim to use across each run of your script, it becomes difficult for your script to know what's the new delim you should use while setting options.

`./examples/mode-option-setting.ts` shows an example of how to swap delims in a somewhat safe way. Try running it with `rofi -show test -modi "test:./examples/mode-option-setting.ts"`
