import { assert } from "https://deno.land/std@0.106.0/testing/asserts.ts";
import { constructOptionMesg } from "../lib/mode-options.ts";

Deno.test("changable ending delim", () => {
  assert(constructOptionMesg("prompt", "🍙", "|"), "\0prompt\x1f🍙|");
});

Deno.test("prints prompt-setting stdout message", () => {
  assert(constructOptionMesg("prompt", "foo"), "\0prompt\x1ffoo\n");
  assert(constructOptionMesg("prompt", "🍙"), "\0prompt\x1f🍙\n");
});

Deno.test("prints message-setting stdout message", () => {
  assert(constructOptionMesg("message", "foo"), "\0message\x1ffoo\n");
  assert(constructOptionMesg("message", "🍙"), "\0message\x1f🍙\n");
  assert(constructOptionMesg("message", "<span class='red'>This is red</span>"), "\0message\x1f<span class='red'>This is red</span>\n");
});

Deno.test("prints markup-rows-setting stdout message", () => {
  assert(constructOptionMesg("markup-rows", true), "\0markup-rows\x1ftrue\n");
  assert(constructOptionMesg("markup-rows", false), "\0markup-rows\x1ffalse\n");
});
