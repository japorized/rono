import { NEW_LINE_CHAR, NULL_CHAR, TERM_SEP_CHAR } from "./constants.ts";

export enum RofiModeOptions {
  /**
   * Targets the prompt message in rofi
   */
  Prompt = "prompt",
  /**
   * Targets the message text in rofi
   */
  Message = "message",
  /**
   *
   */
  MarkupRows = "markup-rows",
  /**
   *
   */
  Urgent = "urgent",
  /**
   *
   */
  Active = "active",
  /**
   *
   */
  Delim = "delim",
  /**
   *
   */
  NoCustom = "no-custom",
  /**
   *
   */
  UseHotKeys = "use-hot-keys",
}

export function constructOptionMesg(
  modeOption: RofiModeOptions | string,
  value: string | boolean,
  delim = NEW_LINE_CHAR,
): string {
  return `${NULL_CHAR}${modeOption}${TERM_SEP_CHAR}${value}${delim}`;
}

function printToStdout(s: string): void {
  const te = new TextEncoder();
  Deno.stdout.writeSync(te.encode(s));
}

/**
 * Set the prompt for the next rofi prompt.
 * @param prompt
 * String for your prompt.
 * @param lineDelim
 * Delimiter to use
 *
 * @examples
 * - Basic usage
 * ```
 * setPrompt("Make a choice");
 * ```
 * - With emojis
 * ```
 * setPrompt("🍙");
 * ```
 */
export function setPrompt(prompt: string, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.Prompt, prompt, lineDelim));
}

/**
 * Set the message for the next rofi prompt.
 * @param mesg
 * String for your message.
 * @param lineDelim
 * Delimiter to use
 *
 * @examples
 * - Using pango markup
 * ```typescript
 * setMessage("<span color='red'>This is red</span>");
 * ```
 */
export function setMessage(mesg: string, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.Message, mesg, lineDelim));
}

/**
 * Set whether markup is enabled for rows.
 * @param bool
 * Whether to set the option to true or not. Set to true to enable markup for rows.
 * @param lineDelim
 * Delimiter to use
 */
export function setMarkupRows(bool: boolean, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.MarkupRows, bool, lineDelim));
}

/**
 * Set the rows that should be marked as urgent
 * @param s
 * String that tells rofi which rows should be marked as urgent.
 * See urgent option for dmenu mode in `man rofi` for syntax.
 * @param lineDelim
 * Delimiter to use
 */
export function setUrgent(s: string, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.Urgent, s, lineDelim));
}

/**
 * Set the rows that should be marked as active
 * @param s
 * String that tells rofi which rows should be marked as active.
 * See active option for dmenu mode in `man rofi` for syntax.
 * @param lineDelim   Delimiter to use
 */
export function setActive(s: string, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.Active, s, lineDelim));
}

/**
 * Set the delimiter for all upcoming rows in rofi
 * @param delim   The new delimiter. Note that default delimiter is '\n'.
 * @param lineDelim   Delimiter to use
 */
export function setDelim(newDelim: string, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.Delim, newDelim, lineDelim));
}

/**
 * Set true to turn off custom inputs
 * @param bool   Whether to turn off custom inputs or not
 * @param lineDelim   Delimiter to use
 */
export function setNoCustom(bool: boolean, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.NoCustom, bool, lineDelim));
}

/**
 * Set true to turn on usage of custom hotkeys
 * @param bool   Whether to turn on custom hotkeys or not
 * @param lineDelim   Delimiter to use
 */
export function useHotKeys(bool: boolean, lineDelim?: string): void {
  printToStdout(constructOptionMesg(RofiModeOptions.UseHotKeys, bool, lineDelim));
}
