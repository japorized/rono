#!/usr/bin/env -S deno run --allow-read --allow-write

import { setDelim, setMessage, setPrompt } from "../mod.ts";

if (Deno.args.length) {
  const value = Deno.args.join(" ");
  if (value === "quit") {
    setPrompt("😢");
    setMessage("Really quit?");
    console.log("yes");
    console.log("no");
    Deno.exit(0);
  }

  if (value === "yes") {
    Deno.exit(0);
  }

  if (value === "try different delim") {
    const delim = "|";
    setDelim(delim);
    setMessage("Make some choice", delim);
    const te = new TextEncoder();
    Deno.stdout.writeSync(te.encode("opt1|opt2|opt3|"));
    setDelim("\\n", delim);
    Deno.exit(0);
  }
}

setPrompt("🍙");
setMessage("<span color='cyan'>Hello world!</span>");
console.log("reload");
console.log("try different delim");
console.log("quit");
